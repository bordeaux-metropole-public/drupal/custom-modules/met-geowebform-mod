<?php

namespace Drupal\geo_webform\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Render\Element;
use Drupal\Core\Form\FormStateInterface;
use Drupal\geofield_map_ext\Element\GeofieldMapExt;
use Drupal\webform\Element\WebformCompositeBase;

/**
 * Provides a 'geo_web'.
 *
 * Webform elements are just wrappers around form elements, therefore every
 * webform element must have correspond FormElement.
 *
 * Below is the definition for a custom 'geo_element' which just
 * renders a simple text field.
 *
 * @FormElement("geo_element")
 *
 * @see \Drupal\Core\Render\Element\FormElement
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21Element%21FormElement.php/class/FormElement
 * @see \Drupal\Core\Render\Element\RenderElement
 * @see https://api.drupal.org/api/drupal/namespace/Drupal%21Core%21Render%21Element
 * @see \Drupal\geo_webform\Element\GeoElement
 */
class GeoElement extends WebformCompositeBase {

  /**
   * @return array[]
   */
  public static function getComponents() {
    return [
      'lat' => [
        'title' => t('Latitude'),
        'range' => 90,
      ],
      'lon' => [
        'title' => t('Longitude'),
        'range' => 180,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#process' => [
        [$class, 'processGeoElement'],
        [$class, 'processAjaxForm'],
      ],
      '#element_validate' => [
        [$class, 'validateWebformComposite'],
      ],
      '#pre_render' => [
        [$class, 'preRenderGeoElement'],
      ],
      '#map_library' => 'leaflet',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getCompositeElements(array $element) {
    $elements = [];
    foreach (static::getComponents() as $name => $component) {
      $elements[$name] = [
        '#type' => 'textfield',
        '#title' => $component['title'],
      ];
    }
    $elements['geocode'] = [
      '#type' => 'search',
      '#title' => t('Geocode'),
    ];
    return $elements;
  }

  /**
   * Processes a 'geo_element' element.
   */
  public static function processGeoElement(&$element, FormStateInterface $form_state, &$complete_form) {
    // Add some NULL properties to avoid "undefined index" notices in the logs.
    if (!isset($element['#gmap_places'])) {
      $element['#gmap_places'] = NULL;
    }
    if (!isset($element['#gmap_places_options'])) {
      $element['#gmap_places_options'] = NULL;
    }
    if (!isset($element['#map_type_selector'])) {
      $element['#map_type_selector'] = NULL;
    }
    if (!isset($element['#click_to_find_marker'])) {
      $element['#click_to_find_marker'] = NULL;
    }
    if (!isset($element['#click_to_place_marker'])) {
      $element['#click_to_place_marker'] = NULL;
    }
    if (!isset($element['#gmap_geocoder'])) {
      $element['#gmap_geocoder'] = NULL;
    }
    if (!isset($element['#gmap_api_key'])) {
      $element['#gmap_api_key'] = NULL;
    }
    if (!isset($element['#map_type'])) {
      $element['#map_type'] = NULL;
    }
    if (!isset($element['#geolocation'])) {
      $element['#geolocation'] = NULL;
    }

    $element = GeoFieldMapExt::latLonProcess($element, $form_state, $complete_form);

    $element['#wrapper_attributes'] = NestedArray::mergeDeep(
      $element['#wrapper_attributes'] ?? [], $element['#attributes'] ?? []);

    return $element;
  }

  /**
   * Prepares a #type 'email_multiple' render element for theme_element().
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for theme_element().
   */
  public static function preRenderGeoElement(array $element) {
    $element = parent::preRenderCompositeFormElement($element);
    $element['#attributes']['type'] = 'geofield_map';
    Element::setAttributes($element, ['id']);
    // static::setAttributes($element, ['form-text', 'geo-element']);.
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function validateWebformComposite(&$element, FormStateInterface $form_state, &$complete_form) {
    // Is this the best way to remove that unwanted 'map' value from the form
    // state? If we don't do this, it gets persisted in webform submission data
    // as an 'Array' string.
    $value = $form_state->getValue($element['#name']);
    if (isset($value['map'])) {
      if (isset($value['map']['geocode'])) {
        $value['geocode'] = $value['map']['geocode'];
      }
      unset($value['map']);
      $form_state->setValue($element['#name'], $value);
    }

    $error_label = $element['#error_label'] ?? $element['#title'];
    foreach (static::getComponents() as $key => $component) {
      if (!empty($element[$key]['#value']) && !is_numeric($element[$key]['#value'])) {
        $form_state->setError($element[$key], t('@title: @component_title is not valid.', [
          '@title' => $error_label,
          '@component_title' => $component['title'],
        ]));
      }
      elseif (is_numeric($element[$key]['#value']) && abs($element[$key]['#value']) > $component['range']) {
        $form_state->setError($element[$key], t('@title: @component_title is out of bounds (@bounds).', [
          '@title' => $error_label,
          '@component_title' => $component['title'],
          '@bounds' => '+/- ' . $component['range'],
        ]));
      }
    }

    parent::validateWebformComposite($element, $form_state, $complete_form);
  }

}
